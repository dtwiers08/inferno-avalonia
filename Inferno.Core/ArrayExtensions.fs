﻿module Inferno.Core.ArrayExtensions

let fold2d (folder: int -> int -> 'S -> 'T -> 'S) (state: 'S) (array: 'T[,]) =
            let mutable state = state
            for x in 0 .. Array2D.length1 array - 1 do
                for y in 0 .. Array2D.length2 array - 1 do
                    state <- folder x y state (array.[x, y])
            state