module Inferno.Core.Extensions

module Tuple =
    let mapFst (fn: 'a -> 'b) (tuple: ('a * 'c)): ('b * 'c) =
        let a, b = tuple
        (fn a, b)

    let mapSnd (fn: 'a -> 'b) (tuple: ('c * 'a)): ('c * 'b) =
        let a, b = tuple
        (a, fn b)

    let bindFst (binder: 'a -> ('b * 'c)) (tuple: ('a * 'c)) =
        binder (fst tuple)

    let bindSnd (binder: 'a -> ('b * 'c)) (tuple: ('b * 'a)) =
        binder (snd tuple)

    let bindFstS (binder: 'a -> struct('b * 'c)) (tuple: struct('a * 'c)) =
        let struct (first, _) = tuple
        binder first

    let bindSndS (binder: 'a -> struct('b * 'c)) (tuple: struct('b * 'a)) =
        let struct (_, second) = tuple
        binder second

    let liftFst (fn: ('a * 'b) -> 'c) (tuple: ('a * 'b)): ('c * 'b) =
        (fn tuple, snd tuple)

    let (>>-) = bindFst
    let (>>=) = bindSnd
