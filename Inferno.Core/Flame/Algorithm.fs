module Inferno.Core.Flame.Algorithm

open Inferno.Core.Flame.Helpers
open Inferno.Core.Flame.Types
open Inferno.Core.Flame.Canvas
open Inferno.Core.Flame.Plot

let DefaultProjectProperties = {
    General = {
        Name = "Default"
        Interpolation = Interpolation.Linear
        PaletteInterpolation = Rgb
        HsvRgbPaletteBlend = 0.
        PaletteMode = PaletteMode.Linear
        InterpolationType = InterpolationType.Linear
    }
    Camera = {
        Center = 0., 0.
        Size = 960, 540
        Scale = 1.
        Rotate = 0.
        Zoom = 0.
    }
    Color = {
        Background = 0., 0., 0.
        Brightness = 1.
        Gamma = 2.2
        Vibrancy = 1.0
        Palette = 0
        Hue = 0.
        GammaThreshold = 0.
    }
    Render = {
        Quality = 50.
        Supersample = 1
        TemporalSamples = 0
        Passes = 1
    }
    Environment = {
        Threads = 4
    }
    Parameters = { // Sierpinski Triangle Transforms
        Transforms = [
            {
                Variations = [
                    (VariationType.Linear, 1.0)
                ]
                Affine = CreateAffine 0.5 0. 0. 0. 0.5 0.
                ColorCode = 0.5
                Weight = 1.
                PostTransform = None
            }
            {
                Variations = [
                    (VariationType.Linear, 1.0)
                ]
                Affine = CreateAffine 0.5 0. 1. 0. 0.5 0.
                ColorCode = 0.8
                Weight = 1.
                PostTransform = None
            }
            {
                Variations = [
                    (VariationType.Linear, 1.0)
                ]
                Affine = CreateAffine 0.5 0. 0. 0. 0.5 1.
                ColorCode = 0.1
                Weight = 1.
                PostTransform = None
            }
        ]
        FinalTransform = None
    }
}

let preCalculateProject (props: ProjectProperties): PrecalculatedProjectProperties =
    let zoomFactor = 2. ** props.Camera.Zoom
    let angle = props.Camera.Rotate
    let center = props.Camera.Center
    let x, y = center
    let canvasAffine =
        props.Camera.Scale * zoomFactor * CreateAffine
            (cos(angle))
            (-sin(angle))
            x
            (-sin(angle))
            (-cos(angle))
            (1. - y)
    let inverseAffine =
        (props.Camera.Scale * zoomFactor) ** -1. * CreateAffine
            (cos(angle))
            (sin(angle))
            (-x * cos(angle) - y * sin(angle))
            (sin(angle))
            (-cos(angle))
            (1. - (-y * cos(angle) - x * sin(angle)))
    let isOnCanvas (pt: Coordinate) =
        let transformedPoint = canvasAffine * pt
        match (transformedPoint.[0], transformedPoint.[1]) with
        | x, _ when x < 0. -> false
        | x, _ when x >= float (fst props.Camera.Size) -> false
        | _, y when y < 0. -> false
        | _, y when y >= float (snd props.Camera.Size) -> false
        | _, _ -> true
    {
        General = props.General
        Camera = props.Camera
        Color = props.Color
        Render = props.Render
        Environment = props.Environment
        Parameters = props.Parameters
        Calculated = {
            CanvasAffine = canvasAffine
            InverseAffine = inverseAffine
            IsOnCanvas = isOnCanvas
        }
    }

let plotPoints (props: PrecalculatedProjectProperties) (grid: Plot.RgbaCanvas) =
    let width, height = props.Camera.Size
    let totalNumberOfPointsRequested = uint64 (props.Render.Quality * float width * float height)
    let successRate = plotMultiple props (InputAmount 10000) |> Array.length |> float |> (/) 10000.
    if successRate = 0. then
        (props, grid)
    else
        for i = 0 to 99 do
            let tempPoints = plotMultiple props (OutputAmount (int (totalNumberOfPointsRequested / 100UL)))
            do
                tempPoints
                |> Array.map (fun coloredPoint ->
                    let color = Color.getColor props coloredPoint.Color
                    let colorCoord: Plot.PalettedCoordinate =
                        {
                            Point = coloredPoint.Point
                            Red = color.Red
                            Green = color.Green
                            Blue = color.Blue
                        }
                    colorCoord
                    )
                |> Array.map (fun value -> { value with Point = props.Calculated.CanvasAffine * value.Point })
                |> Array.iter (fun value ->
                    let row, column = int value.Point.[1], int value.Point.[0]
                    let newRgbValue: Plot.GriddedRgbaValues = {
                        Count = 0. // this is unused; I just don't feel like declaring yet another point type.
                        Red = value.Red
                        Green = value.Green
                        Blue = value.Blue
                    }
                    plotPoint grid (row, column) newRgbValue)

        (props, grid)

let postProcess (props: PrecalculatedProjectProperties) (grid: RgbaCanvas) =
    let newGrid =
        grid
        |> Color.logScale
        |> Color.gamma props.Color.Gamma
    (props, grid)

let toSKColor (props: PrecalculatedProjectProperties) grid =
    (props, grid)

let toImage (props: PrecalculatedProjectProperties) grid =
    grid

let render props =
    let grid = mkCanvas props
    (props, grid)
    ||> plotPoints
    ||> postProcess
    ||> toSKColor
    ||> toImage