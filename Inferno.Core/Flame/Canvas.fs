﻿module Inferno.Core.Flame.Canvas

open Inferno.Core.Flame.Types
open Inferno.Core.ArrayExtensions

type RgbaCanvas = Plot.Canvas<Plot.GriddedRgbaValues>

let mkCanvas (props: PrecalculatedProjectProperties) : RgbaCanvas =
    let zeroedPlotPoint: Plot.GriddedRgbaValues = {Count = 0.; Color = {Red = 0.; Green = 0.; Blue = 0.;}}
    let width, height = props.Camera.Size
    Array2D.create width height zeroedPlotPoint

let getQuality (grid: RgbaCanvas) =
    (grid |> fold2d (fun _ _ accum elem -> accum + elem.Count) 0.) /
        float ((grid |> Array2D.length1) * (grid |> Array2D.length2))

let plotPoint (grid: RgbaCanvas) (rowColumn: int * int) (point: Plot.GriddedRgbaValues) =
    let row, column = rowColumn
    let current = grid.[row, column]
    grid.[row, column] <- ({
        Count = current.Count + 1.
        Color = {
            Red = current.Color.Red + point.Color.Red
            Green = current.Color.Green + point.Color.Green
            Blue = current.Color.Blue + point.Color.Blue
        }
    })