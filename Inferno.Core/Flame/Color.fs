module Inferno.Core.Flame.Color

open Inferno.Core.Flame.Types
open Inferno.Core.Flame.Palette
open System
open SkiaSharp

type Channel = Red | Green | Blue

let toHsv (rgbValue: RgbValue) =
    let r, g, b = rgbValue.Red, rgbValue.Green, rgbValue.Blue
    let cMax = Math.Max(b, Math.Max(r, g))
    let cMin = Math.Min(b, Math.Max(r, g))
    let delta = cMax - cMin
    let dominantColor =
        match cMax with
        | _ when cMax - r = 0. -> Red
        | _ when cMax - g = 0. -> Green
        | _ -> Blue
    let v = cMax
    let s =
        match cMax with
        | 0. -> 0.
        | _ -> delta / cMax
    let h =
        match delta with
        | 0. -> 0.
        | _ ->
            let result =
                match dominantColor with
                | Red -> ((g - b) / delta) % 6.
                | Green -> ((b - r) / delta) + 2.
                | Blue -> ((r - g) / delta) + 4.
            result / 6.
    {
        Hue = h
        Saturation = s
        Value = v
    }

let toRgb (hsvValue: HsvValue) =
    let h, s, v = hsvValue.Hue, hsvValue.Saturation, hsvValue.Value
    let c = v * s
    let x = c * (1. - Math.Abs(h * 6. % 2. - 1.))
    let m = v - c
    let r', g', b' =
        match Math.Floor((h * 6.) % 6.) with
        | 0. -> (c, x, 0.)
        | 1. -> (x, c, 0.)
        | 2. -> (0., c, x)
        | 3. -> (0., x, c)
        | 4. -> (x, 0., c)
        | 5. -> (c, 0., x)
        | _ -> failwith "hue out of bounds"
    {
        Red = r' + m
        Green = g' + m
        Blue = b' + m
    }




let getColor (props: PrecalculatedProjectProperties) (colorCode: float) =
    let firstGreaterPoint = fun ((a, b): Color.PaletteElement * Color.PaletteElement) -> b.Point >= colorCode
    let palette = Palettes |> Array.tryItem props.Color.Palette |> function
        | Some p -> p
        | None -> failwith  (sprintf "Palette code out of range: %i" props.Color.Palette)
    let lower, upper =
        palette.Colors
        |> List.pairwise
        |> List.find firstGreaterPoint
    match colorCode with
    | l when l = lower.Point -> lower
    | u when u = upper.Point -> upper
    | coord ->
        let paletteMode = props.General.PaletteMode
        let distance = upper.Point - lower.Point
        let lowDistance, highDistance = coord - lower.Point, upper.Point - coord
        match paletteMode with
        | Step ->
            match highDistance - lowDistance with
            | closerToHigher when closerToHigher > 0. -> upper
            | closerToLower when closerToLower < 0. -> lower
            | _ -> upper
        | PaletteMode.Linear ->
            let normalizedLowDistance = lowDistance / distance
            let normalizedHighDistance = highDistance / distance
            let blendedAverage lowerValue upperValue =
                (normalizedLowDistance * lowerValue) + (normalizedHighDistance * upperValue)
            {
                Point = colorCode
                Red = blendedAverage lower.Red upper.Red
                Green = blendedAverage lower.Green upper.Green
                Blue = blendedAverage lower.Blue upper.Blue
            }

let logScale (grid: Plot.RgbaCanvas): Plot.RgbaCanvas =
    grid
    |> Array2D.map (fun pixel ->
        let logAlpha = Math.Log10 pixel.Count
        let multiplier = logAlpha / pixel.Count
        let red = (pixel.Color.Red * multiplier)
        let green = (pixel.Color.Green * multiplier)
        let blue = (pixel.Color.Blue * multiplier)
        {
            Count = logAlpha
            Color = {
                Red = red
                Green = green
                Blue = blue
            }
        })

let gamma gammaValue (grid: Plot.RgbaCanvas) : Plot.RgbaCanvas =
    let gammaFunction (value: float) = value ** (1. / gammaValue)
    grid
    |> Array2D.map (fun pixel ->
        {
            Count = pixel.Count
            Color = {
                Red = gammaFunction pixel.Color.Red
                Green = gammaFunction pixel.Color.Green
                Blue = gammaFunction pixel.Color.Blue
            }
        })

let toColor (grid: Plot.RgbaCanvas) =
    let toByte number = byte (Math.Clamp ((number * 256.), 0., 255.))
    grid
    |> Array2D.map (fun pixel ->
        SKColor(toByte pixel.Color.Red, toByte pixel.Color.Green, toByte pixel.Color.Blue, toByte pixel.Count))