module Inferno.Core.Flame.Helpers

open System
open Inferno.Core.Flame.Types

let Point x y = Coordinate.Build.DenseOfArray([| x; y; 1.|])

// Creation of Affine Coefficients
let CreateAffine a b c d e f = AffineMatrix.Build.DenseOfRowMajor(2, 3, [| a; b; c; d; e; f |])

let UnitAffine = CreateAffine 1. 0. 0. 0. 1. 0.

let AffinizePoint (affine: AffineMatrix) (pt: Coordinate) = affine.RemoveColumn(2) * pt + affine.Column(2)

let rand = Random()

let trunc (input: float) =
        if Math.Sign input > 0 then Math.Floor input
        else Math.Ceiling input


let Psi() = rand.NextDouble()

let Lambda() =
    if rand.Next(1) = 0 then -1.0
    else 1.

let Omega() =
    if rand.Next(1) = 0 then 0.
    else Math.PI

let inline R(pt: Coordinate) = sqrt (pt.[0] ** 2. + pt.[1] ** 2.)

// r squared - shortcut for performance
let inline R2 (pt: Coordinate) = pt.[0] ** 2. + pt.[1] ** 2.
let inline Theta (pt: Coordinate) = Math.Atan(pt.[0] / pt.[1])
let inline Phi (pt: Coordinate) = Math.Atan(pt.[1] / pt.[0])

let inline X(pt: Coordinate) = pt.[0]

let inline Y(pt: Coordinate) = pt.[1]

let inline A(affine: AffineMatrix) = affine.[0, 0]
let inline B(affine: AffineMatrix) = affine.[0, 1]
let inline C(affine: AffineMatrix) = affine.[0, 2]
let inline D(affine: AffineMatrix) = affine.[1, 0]
let inline E(affine: AffineMatrix) = affine.[1, 1]
let inline F(affine: AffineMatrix) = affine.[1, 2]

let sin = Math.Sin
let cos = Math.Cos
let tan = Math.Tan
let sinh = Math.Sinh
let cosh = cosh
let pi = Math.PI


// Dealing with this later, once I reinstate more Variations.
//let CreateBlobParams high low waves = [|high; low; waves;|]: float array

