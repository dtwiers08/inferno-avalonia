module Inferno.Core.Flame.Jobs

open System
open Inferno.Core.Flame.Types
open Inferno.Core.Flame.Canvas
open Hopac

type Status = Plotting | Filtering | Done | Cancelled

type StatusInfo = {
    Status: Status
    PercentDone: float
    PointsPlotted: float
    ElapsedTime: TimeSpan
    EstimatedTimeLeft: TimeSpan
}

type Request =
    | GetStatus
    | GetWorkerCommand
    | PlotPoint of String

let renderProject props =
    job {
        let grid = mkCanvas props

        return grid
    }