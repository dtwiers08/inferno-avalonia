﻿module Inferno.Core.Flame.Palette

open System.IO
open Inferno.Core.Flame.Types
open FSharp.Data

type PaletteXml = XmlProvider<"../flam3-palettes.xml">

let Palettes: Color.Palette array =
    let content = PaletteXml.Parse(File.ReadAllText("../flam3-palettes.xml"))
    let parseRgbString (inputString: string) : Color.PaletteElement list =
        let toInt str = System.Convert.ToInt32(str, 16)
        let chunkedInputString =
            inputString
            |> Seq.filter (fun character -> Seq.contains character ("0123456789abcdef".ToCharArray()))
            |> Seq.chunkBySize 8
        let colorCount = chunkedInputString |> Seq.length
        chunkedInputString
        |> Seq.map (fun rgbValue ->
            rgbValue
            |> Seq.skip 2
            |> Seq.chunkBySize 2
            |> Seq.map (System.String >> toInt >> float)
            |> Seq.map (fun value -> value / 256.)
            )
        |> Seq.map (fun floatSequence ->
            match floatSequence |> List.ofSeq with
            | [r; g; b;] -> (r, g, b)
            | _ -> failwith "palette parsing failed"
            )
        |> Seq.mapi (fun (index: int) ((r, g, b): float * float * float) ->
            let result: Color.PaletteElement = {
                Point = (float index / (float colorCount))
                Red = r
                Green = g
                Blue = b
            }
            result
            )
        |> Seq.toList

    let resultValue =
        content.Palettes
        |> Array.map (fun (paletteFromXml: PaletteXml.Palette) ->
            let result: Color.Palette = {
                    Name = paletteFromXml.Name.Value
                    Colors = parseRgbString paletteFromXml.Data
                }
            result
        )
    do printfn "%A" resultValue
    resultValue
