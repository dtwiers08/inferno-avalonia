﻿module Inferno.Core.Flame.Plot

open System
open Inferno.Core.Flame.Types
open Inferno.Core.Flame.Helpers
open Inferno.Core.Flame.Variations

type [<Struct>] ColoredPoint = {
    Point: Coordinate
    Color: float
}

let defaultTransform: Transform = {
    Affine = CreateAffine 0. 0. 0. 0. 0. 0.
    Variations = [(VariationType.Linear, 0.0)]
    ColorCode = 0.
    PostTransform = None
    Weight = 1.0
}

let executeTransforms (affine: AffineMatrix) (variations: (VariationType * float) list) (post: AffineMatrix option) (pt: Coordinate) =
    let calculateVariations translatedPoint =
        variations
        |> List.map (fun (variationType, coefficient) ->
            let variation = getVariation variationType
            coefficient * (variation translatedPoint) )
        |> List.fold (+) (Point 0. 0.)
    let evaluatePost =
            match post with
            | None -> id
            | Some t -> (*) t
    let translatePoint = (*) affine
    pt
    |> translatePoint
    |> calculateVariations
    |> evaluatePost

let inline pointGenerator props =
    let rand = Random()
    // Begin random weighting system
    let accumulatedWeights =
        props.Parameters.Transforms
        |> List.map (fun elem -> elem.Weight)
        |> List.scan (+) 0.
    let weightSum =
        accumulatedWeights |> List.last |> function
        | 0. -> None // Uh oh, we have no weights in any of our transforms!
        | x -> Some x
    fun (previousPoint: ColoredPoint) ->
        let chosenTransform =
            weightSum |> function
            | None -> defaultTransform
            | Some x ->
                let randomDouble = rand.NextDouble() * x
                (accumulatedWeights, props.Parameters.Transforms)
                ||> List.zip
                |> List.find (fun (weight, _) -> weight > randomDouble)
                |> snd
        let destination =
            executeTransforms
                chosenTransform.Affine
                chosenTransform.Variations
                chosenTransform.PostTransform
                previousPoint.Point
        {
            Point = destination
            Color = (previousPoint.Color / 2.) + (chosenTransform.ColorCode / 2.)
        }

type PointCount =
    | InputAmount of int // start with int elements
    | OutputAmount of int // end up with int elements - only use if you know you'll eventually get there!

let plotMultiple (props: PrecalculatedProjectProperties) pointCount =
    let rand = Random()
    let mutable initialPoint = { Color = 0.; Point = Point (rand.NextDouble()) (rand.NextDouble()) }
    let generatePoint = pointGenerator props
    let preCalculatedPoint =
        for _ = 0 to 20 do
            initialPoint <- generatePoint initialPoint
        initialPoint

    match pointCount with
    | InputAmount x ->
        Array.create x preCalculatedPoint
        |> Array.scan (fun state _ -> generatePoint state) preCalculatedPoint
        |> Array.map (fun pt ->
            match props.Parameters.FinalTransform with
            | None -> pt
            | Some ft ->
                {
                    Point = executeTransforms ft.Affine ft.Variations ft.PostTransform pt.Point
                    Color = pt.Color
                }
        )
        |> Array.filter (fun elem -> props.Calculated.IsOnCanvas elem.Point)
    | OutputAmount x ->
        let mutable workingPoint = preCalculatedPoint
        Array.init x (fun _ ->
            while not (props.Calculated.IsOnCanvas workingPoint.Point) do
                workingPoint <- generatePoint workingPoint
            workingPoint
            )
