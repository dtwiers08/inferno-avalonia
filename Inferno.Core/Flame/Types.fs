﻿namespace Inferno.Core.Flame.Types

open MathNet.Numerics.LinearAlgebra

type Coordinate = Vector<float>

type Variation = Coordinate -> Coordinate

type AffineMatrix = Matrix<float> // I can't enforce this, just always make it a 2x3.

type DependentVariation = AffineMatrix -> Coordinate -> Coordinate

type ParametricVariation = float array -> AffineMatrix -> Coordinate -> Coordinate

type VariationType =
    | Linear
    | Sinusoidal
    | Spherical
    (* The above 3 are all I'm starting with for now until I can at least get the Sierpinski Triangle working *)
//    | Swirl
//    | Horseshoe
//    | Polar
//    | Handkerchief
//    | Heart
//    | Disc
//    | Spiral
//    | Hyperbolic
//    | Diamond
//    | Ex
//    | Julia
//    | Bent
//    | Waves
//    | Fisheye
//    | Popcorn
//    | Exponential
//    | Power
//    | Cosine
//    | Rings
//    | Fan
//    | Blob

type [<Struct>] RgbValue = {
    Red: float
    Green: float
    Blue: float
}

type [<Struct>] HsvValue = {
    Hue: float
    Saturation: float
    Value: float
}

module Plot =


    type [<Struct>] ColorCodedCoordinate = {
        Point: Coordinate
        ColorCode: float
    }

    type Alpha = float

    type PalettedCoordinate = {
        Point: Coordinate
        Color: RgbValue
    }
    type GriddedRgbaValues = {
        Count: float
        Color: RgbValue
    }

    type Canvas<'a> = 'a[,]

    type RgbaCanvas = Canvas<GriddedRgbaValues>

module Color =
    type PaletteElement = {
        Point: float
        Red: float
        Blue: float
        Green: float
    }

    type Palette = {
        Name: string
        Colors: PaletteElement list
    }

// https://github.com/scottdraves/flam3/wiki/XML-File-Format
type Interpolation = Smooth | Linear
type PaletteInterpolation = HsvCircular | Hsv | Rgb | Sweep
type PaletteMode = Step | Linear
type InterpolationType = Log | Linear | Old
type GeneralProperties = {
    Name: string
    Interpolation: Interpolation
    PaletteInterpolation: PaletteInterpolation
    HsvRgbPaletteBlend: float
    PaletteMode: PaletteMode
    InterpolationType: InterpolationType
}

type CameraProperties = {
    Center: float * float
    Size: int * int
    Scale: float
    Rotate: float
    Zoom: float
}

type ColorProperties = {
    Background: float * float * float
    Brightness: float
    Gamma: float
    Vibrancy: float
    Palette: int
    Hue: float
    GammaThreshold: float
}

type RenderingProperties = {
    Quality: float
    Supersample: int
    TemporalSamples: int
    Passes: int
}

type FinalTransform = {
    Affine: AffineMatrix
    Variations: (VariationType * float) list
    PostTransform: AffineMatrix option
}
type Transform = {
    Variations: (VariationType * float) list
    ColorCode: float
    Affine: AffineMatrix
    PostTransform: AffineMatrix option
    Weight: float
}

type ParameterProperties = {
    Transforms: Transform list
    FinalTransform: FinalTransform option
}

type EnvironmentProperties = {
    Threads: int
}

type CalculatedProperties = {
    CanvasAffine: AffineMatrix
    InverseAffine: AffineMatrix
    IsOnCanvas: Coordinate -> bool
}

type ProjectProperties = {
    General: GeneralProperties
    Camera: CameraProperties
    Color: ColorProperties
    Render: RenderingProperties
    Environment: EnvironmentProperties
    Parameters: ParameterProperties
}

type PrecalculatedProjectProperties = {
    General: GeneralProperties
    Camera: CameraProperties
    Color: ColorProperties
    Render: RenderingProperties
    Environment: EnvironmentProperties
    Parameters: ParameterProperties
    Calculated: CalculatedProperties
}
