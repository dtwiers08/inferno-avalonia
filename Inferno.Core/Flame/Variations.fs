module Inferno.Core.Flame.Variations

open Inferno.Core.Flame.Types
open Inferno.Core.Flame.Helpers
open System




let Linear: Variation = fun pt -> pt

let Sinusoidal: Variation = fun pt -> sin |> pt.Map

let Spherical: Variation =
    fun pt ->
        let r2 = R2 pt
        (1. / r2) * pt

let Swirl: Variation =
    fun pt ->
        let r2, x, y = R2 pt, X pt, Y pt
        Point (x * sin r2 - y * cos r2) (x * cos r2 + y * sin r2)

let Horseshoe: Variation =
    fun pt ->
        let r, x, y = R pt, X pt, Y pt
        (1. / r) * Point ((x - y) * (x + y)) (2. * x * y)

let Polar: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        Point (theta / pi) (r - 1.)

let Handkerchief: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        r * Point (sin(theta + r)) (cos(theta - r))

let Heart: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        r * Point (sin(theta * r)) (-cos(theta * r))

let Disc: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        (theta / pi) * Point (sin(pi * r)) (cos(pi * r))

let Spiral: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        (r ** -1.0) * Point (cos theta + sin r) (sin theta - cos r)

let Hyperbolic: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        Point ((sin theta) / r) (r * cos theta)

let Diamond: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        Point ((sin theta) * (cos r)) ((cos theta) * (sin r))

let Ex: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        let p0 = sin(theta + r) ** 3.0
        let p1 = cos(theta - r) ** 3.0
        r * Point (p0 + p1) (p0 - p1)

let Julia: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        let operand = theta / 2.0 + Omega()
        (sqrt r) * Point (cos(operand)) (sin(operand))

let Bent: Variation =
    fun pt ->
        let x, y = X pt, Y pt
        let xSign = Math.Sign x
        let ySign = Math.Sign y
        pt.PointwiseMultiply
            (match xSign, ySign with
             | -1, -1 -> Point 2. 0.5
             | _, -1 -> Point 1. 0.5
             | -1, _ -> Point 2. 1.
             | _, _ -> Point 1. 1.)

let Waves: DependentVariation =
    fun affine pt ->
        let b, c, e, f, x, y = B affine, C affine, E affine, F affine, X pt, Y pt
        pt + Point (b * sin(y / c ** 2.)) (e * sin(x / f ** 2.))

let Fisheye: Variation =
    fun pt ->
        let r, x, y = R pt, X pt, Y pt
        (2.0 / (r + 1.0)) * Point (y) (x)

let Popcorn: DependentVariation =
    fun affine pt ->
        let c, f, x, y = C affine, F affine, X pt, Y pt
        Point (x + c * sin(tan(3. * y))) (y + f * sin(tan(3. * x)))

let Exponential: Variation =
    fun pt ->
        let x, y = X pt, Y pt
        Math.E ** (x - 1.0) * Point (cos(pi * y)) (sin(pi * y))

let Power: Variation =
    fun pt ->
        let r, theta = R pt, Theta pt
        r ** (sin theta) * Point (cos theta) (sin theta)

let Cosine: Variation =
    fun pt ->
        let x, y = X pt, Y pt
        Point (cos(pi * x) * cosh(y)) (-sin(pi * x) * sinh(y))

let Rings: DependentVariation =
    fun affine pt ->
        let c2, r, theta = (C affine) ** 2., R pt, Theta pt
        let multiplier = (r + c2) % (2. * c2) - c2 + r * (1. - c2)
        multiplier * Point (cos theta) (sin theta)

let Fan: DependentVariation =
    fun affine pt ->
        let c, f, r, theta = C affine, F affine, R pt, Theta pt
        let t = pi * c ** 2.
        let overHalf = (theta + f) % t > t / 2.
        let op: float -> float -> float =
            if overHalf then (-)
            else (+)
        r * Point (cos(op theta t / 2.0)) (sin(op theta t / 2.0))

let Blob: ParametricVariation =
    fun blobParams _ pt ->
        let high, low, waves = blobParams.[0], blobParams.[1], blobParams.[2]
        let r, theta = R pt, Theta pt
        let multiplier = r * (low + (high - low) / 2. * (sin(waves * theta) + 1.))
        multiplier * Point (cos theta) (sin theta)

let getVariation variationType =
    match variationType with
    | VariationType.Linear -> Linear
    | VariationType.Sinusoidal -> Sinusoidal
    | VariationType.Spherical -> Spherical