namespace Inferno

open Avalonia.Layout
open Avalonia.Media.Imaging

/// This is the main module of your application
/// here you handle all of your child pages as well as their
/// messages and their updates, useful to update multiple parts
/// of your application, Please refer to the `view` function
/// to see how to handle different kinds of "*child*" controls
module Shell =
    open Elmish
    open Avalonia
    open Avalonia.Controls
    open Avalonia.Input
    open Avalonia.FuncUI
    open Avalonia.FuncUI.Components.Hosts
    open Avalonia.FuncUI.DSL
    open Avalonia.FuncUI.Elmish

    type State = {
        random: string
        window: HostWindow
    }

    type Msg =
        | ExitApplication
        | Render of string

    let init (window: HostWindow) =
        { random = "test"; window = window },
        Cmd.none

    let update (msg: Msg) (state: State): State * Cmd<_> =
        match msg with
        | ExitApplication ->
            do state.window.Close()
            state, Cmd.none
        | Render _ -> state, Cmd.none

    let view (state: State) (dispatch: Dispatch<Msg>) =
        DockPanel.create [
            DockPanel.lastChildFill false
            DockPanel.children [
                Menu.create [
                    DockPanel.dock Dock.Top
                    Menu.zIndex 1000
                    Menu.viewItems [
                        MenuItem.create [
                            MenuItem.header "_File"
                            MenuItem.zIndex 1000
                            MenuItem.viewItems [
                                MenuItem.create [
                                    MenuItem.header "_Quit"
                                    MenuItem.showAccessKey true
                                    MenuItem.hotKey (KeyGesture.Parse "Ctrl+Q")
                                    MenuItem.onClick (fun _ -> dispatch ExitApplication)
                                    MenuItem.onKeyDown (fun k ->
                                        match k.Key, k.KeyModifiers with
                                        | Key.Q, KeyModifiers.Control -> dispatch ExitApplication
                                        | _, _ -> ())
                                ]
                            ]
                        ]
                    ]
                ]
                Button.create [
                    DockPanel.dock Dock.Top
                    Button.content "Render"
                    Button.onClick (fun _ -> () )
                ]
                StackPanel.create [
                    StackPanel.horizontalAlignment HorizontalAlignment.Center
                    StackPanel.verticalAlignment VerticalAlignment.Center
                    StackPanel.children [
                        Image.create [
                            DockPanel.dock Dock.Top
                            Image.width 640.
                            Image.height 426.
                            Image.source (new Bitmap("../../../sample_640_426.bmp"))
                        ]
                    ]
                ]
            ]
        ]

    /// This is the main window of your application
    /// you can do all sort of useful things here like setting heights and widths
    /// as well as attaching your dev tools that can be super useful when developing with
    /// Avalonia
    type MainWindow() as this =
        inherit HostWindow()
        do
            base.Title <- "Inferno"
            base.Width <- 1024.0
            base.Height <- 768.0
            base.MinWidth <- 800.0
            base.MinHeight <- 600.0
            base.AttachDevTools()
            this.VisualRoot.VisualRoot.Renderer.DrawFps <- true
//            this.VisualRoot.VisualRoot.Renderer.DrawDirtyRects <- true
            Elmish.Program.mkProgram (fun () -> init (this :> HostWindow)) update view
            |> Program.withHost this
            |> Program.run
